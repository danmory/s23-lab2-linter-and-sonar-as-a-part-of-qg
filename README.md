# Lab 2 -- Linter and SonarQube as a part of quality gates

[![pipeline status](https://gitlab.com/danmory/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)](https://gitlab.com/danmory/s23-lab2-linter-and-sonar-as-a-part-of-qg/-/commits/master)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=danmory_s23-lab2-linter-and-sonar-as-a-part-of-qg&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=danmory_s23-lab2-linter-and-sonar-as-a-part-of-qg)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=danmory_s23-lab2-linter-and-sonar-as-a-part-of-qg&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=danmory_s23-lab2-linter-and-sonar-as-a-part-of-qg)

## Homework

SonarQube checks:

![sonarqube](./assets/sonarqube.png)

SonarCloud:

![sonarcloud](./assets/sonarcloud.png)
